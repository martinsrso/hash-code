# Hash Backend Challenge
## Estrutura
A estrutura básica do projeto está da seguinte forma

### Database
Está sendo utilizando um banco Postgres rodando na porta `7001` na versão `11`.

### Hash Products API
Restful API escrita em Golang que rodará na porta `8888`.

####  Products
`GET [/products]` - Retorna uma lista de produtos pré-cadastrados. Pode-se passar X-USER-ID no header para a identificação de um usuário cadastrado no banco.

```json
[
	{
		"title": "iphone",
		"description": "celular",
		"price_in_cents": 100
	}
]
```

### Discount GRPC
Serviço sem acesso ao banco escrito em javascript e escutando na porta `2019`.

### Executando o Projeto
Para começar execute no terminal:
```bash
$ docker-compose up --build -d 
```

### Testes
Para rodar os testes basta executar o comando para cada serviço:

Para testes da API Restful
```bash
$ make test
```

Para testes do Discount service
```bash
$ yarn test
```

## Alterações

Os valores do desconto podem ser alterados utilizando as váriaveis de ambiente do serviço `discount-serveless` como descrito abaixo.

BIRTHDAY_DISCOUNT: 0.05 (5%)
BLACKFRIDAY_DISCOUNT: 0.1 (10%)
BLACKFRIDAY_DAY: 25
BLACKFRIDAY_MONTH: 11
